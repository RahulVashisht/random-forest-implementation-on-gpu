#include<stdio.h>
#include<string.h>
#include<math.h>
#include<time.h>
#include<stdlib.h>
#define forest 100
#define N1 945
#define N2 316
#define W 13
 float findk_t=0.0,sum_t=0.0,max_t=0.0,entropy_t=0.0,temp_t=0.0;
clock_t start1, end1;
int hi=0;
 
struct deci_tree
{
	int  colname; //attrbute name
	int result;  // class
	int childno;  // no of child
	int value;   // for classification value
	struct deci_tree *c[100]; // all child
	int ischild;  //  for child
}root;

void findk(int **a, int **b,int obj,int attr)
{
    int i,j,k,count = 1;
    int flag  =0 ;   
    for(k = 0; k < attr;k++)
    { 
        b[k][1] = a[1][k];
        i = 1; 
        while(i < obj){
            for(j = 1 ; j < count; j++)
			{
                if(a[i][k] == b[k][j]) 
                {
                    flag = 1;     
                }
            }
            if(flag  == 0 ){
                b[k][count] = a[i][k];
                count++;
            }
            flag =0;
            i++;

        }
        b[k][0] = count-1;

        count = 1;
    }
}

int findmax(double *gain1 , int attr)
{
    int i = 0 ;
	int max1 = 0 ;
    double max;
    max = gain1[0];
    for(i = 1; i < attr; i++ )
	{
        if(gain1[i] > max)
		{
            max1 = i;
            max = gain1[i];
        }
    }
   return max1;
}

double findsum(double *col1,int len)
{
    int i;
    double sum =0;    
    for(i = 0;i < len ;i ++)
	{
        sum =sum +col1[i];
    	}         
    return sum;
}

double entropy(int **a,int obj,int attr,int attrpos,int val,double *temp)
{
    int x;
    x = a[1][attr-1];
    int i;
    double b,c,d;
    double count1 = 0,count2 = 0 ;
    if(attrpos == attr-1)
    {
        for(i=1;i<obj;i++)
        {
            if(a[i][attrpos]==x)
			{
                count1++;
         	}   else
                count2++; 
        }
    }
    if( attrpos != attr-1)
    {
        for(i=1; i<obj; i++)
        {
            if(a[i][attrpos] == val)
            {
                if(a[i][attr-1] == x)      
                    count1 = count1 + 1;

                else
                    count2 = count2 + 1;
            }
        }
    }
    b = count1/(count1+count2);
    c = count2/(count1+count2);

	*temp=count1+count2;
    if(count1 == 0.000000 || count2 == 0.000000) 
        d = 0 ;
    else
        d =  ((count1+count2)/(obj-1)) *(-(b*(log(b)/log(2)) + c*(log(c)/log(2))));
    return d;
}

void infoGainRecursive(int **a,struct deci_tree *parent,int obj,int attr)
{
	int i,j,k,l,m,it;
	int var1;
	double temp;
	
	int **modifiedA = (int **)malloc(100 * sizeof(int *));
	for (it=0; it<100; it++)
	modifiedA[it] = (int *)malloc(100 * sizeof(int));

	int **modifiedB = (int **)malloc(100 * sizeof(int *)); 
	for (it=0; it<100; it++) 
	modifiedB[it] = (int *)malloc(100 * sizeof(int)); 		

    double *col = (double *)malloc(100 * sizeof(double )); 
    double *entr = (double *)malloc(100 * sizeof(double )); 
    double *gain ;//cudaHostAlloc( &gain, 100 * sizeof(double ),0);//
	gain= (double *)malloc(100 * sizeof(double )); 

	temp_t=0.0;
	start1 = clock();
	findk(a,modifiedB,obj,attr);
	end1 = clock();
	temp_t = ((double) (end1 - start1))/CLOCKS_PER_SEC;
	findk_t+=temp_t;

	temp_t=0.0;
	start1 = clock();
	entr[attr-1] = entropy(a,obj,attr,attr-1,0,&temp);
	end1 = clock();
	temp_t = ((double) (end1 - start1))/CLOCKS_PER_SEC;
	entropy_t+=temp_t;

	if(entr[attr-1]!=0 && attr>1)
	{	
			parent->result=3;
    		parent->childno=0;
    		parent->ischild=0;;

		hi++;

	   	for(i= 0; i < attr-1 ; i++)
		{
			for(j= 1 ; j <= modifiedB[i][0]; j++)
			{   

				temp_t=0.0;
				start1 = clock();
				col[j-1] = entropy(a,obj,attr,i,modifiedB[i][j],&temp);
				end1 = clock();
				temp_t = ((double) (end1 - start1))/CLOCKS_PER_SEC;
				entropy_t+=temp_t;

				col[j-1]*=(temp/(obj-1));
				modifiedA[i][j]=temp+10;    
			}

				temp_t=0.0;
				start1 = clock();
		        entr[i] =  findsum(col,modifiedB[i][0]);   
				end1 = clock();
				temp_t = ((double) (end1 - start1))/CLOCKS_PER_SEC;
				sum_t+=temp_t;

		    gain[i] =  entr[attr-1]  - entr[i];   
		}

		temp_t=0.0;
		start1 = clock();
		var1= findmax(gain,attr-1);
		end1 = clock();
		temp_t = ((double) (end1 - start1))/CLOCKS_PER_SEC;
		max_t+=temp_t;	

		for(j= 1 ; j <=modifiedB[var1][0] ; j++)
		{   
			int **arr = (int **)malloc(modifiedA[var1][j] * sizeof(int *)); 
			for (it=0; it<modifiedA[var1][j]; it++) 
			arr[it] = (int *)malloc(attr * sizeof(int)); 
			for(m=0,k=0;m<attr;m++)
			{
				if(m!=var1)
				{
					arr[0][k]=a[0][m];
					k++;
				}
			}
				k=1;
				l=0;
			for(i=1;i<obj;i++)
			{
				if(a[i][var1]==modifiedB[var1][j])
				{
					for( m=0;m<attr;m++)
					{
						if(m!=var1)
						{
							arr[k][l]=a[i][m];
							l++;
						}
					}	
				k++;
				l=0;
				}
			}
			struct deci_tree *temp;
			temp =(struct deci_tree *)malloc(sizeof(struct deci_tree));
			parent->colname=a[0][var1];
			parent->c[parent->childno]=temp;
			parent->c[parent->childno]->value=modifiedB[var1][j];	
			parent->childno++;    

			infoGainRecursive(arr,temp,modifiedA[var1][j]-9,attr-1);
    	}
	free(modifiedB);
	free(modifiedA);
	free(col);
	free(entr);
	free(gain);
    }
	else if(attr>1)
	{
		parent->result=a[1][attr-1];
    	parent->ischild=1;
		free(modifiedB);
		free(modifiedA);
		free(col);
		free(entr);
		free(gain);
	}
	else
	{
		free(modifiedB);
		free(modifiedA);
		free(col);
		free(entr);
		free(gain);
	}
}

void read_input(int  **M,  char *filename)
{
    FILE * ipf = fopen(filename,  "r" );
	char * line = NULL;
    size_t len = 0;
    ssize_t read;
	int k1=0,k2=0,k3=0,k4=24,j;
	while ((read = getline(&line, &len, ipf)) != -1)
	{
		k2=0;
		int sum=0,mul=1;	
		for( j=read-2;j>=-1 ; j--)
		{
			if(line[j]==',' || j<0)
			{
				M[k3][k4]=sum;
				k4--;
				k2++;
				sum=0;
				mul=1;	
			}
			else
			{
				int temp=((int)(line[j]-'0'));
				sum+=(temp*mul);
				mul*=10;
			}
		}
		k1++;
		k3++;
		k4=24;
	}
}

int classification(int **test,int obj, int attr,struct deci_tree *parent)
{

	if(parent->ischild==1)
	{
		return(parent->result);
	}
	else
	{
		int index=0;
		for(;index<attr;index++)
		{
			if(parent->colname==test[0][index])
			{
				break;
			}
		}
		int i;
		for( i=0; i<parent->childno; i++)		
		{
			if(parent->c[i]->value==test[obj][index])
			{
				classification(test,obj,attr,parent->c[i]);

				break;
			}
		}

	}
}

void attr_perm(int **a, int b[forest][26], int attr)
{
	int i,j,temp;//,arr[24];

	for(i=0;i<forest;i++)
	{
		b[i][W-1]=a[0][24];
	}
	for(i=0;i<forest;i++)
	{
		for(j=0;j<(W-1);j++)
		{
			temp=rand();
			temp=temp%2;
			temp+=(2*j);
			b[i][j]=temp+100;
		}
	}
}

void randomfile(int **a,int **a_rand,int b[forest][26],int m,int obj,int attr)
{
	int i,j,k,num, upper=obj-1,lower=1;
	for(i=0;i<=(W-1);i++)
	a_rand[0][i]=b[m][i];
	
	for(i=1;i<obj;i++)
	{
		k=0;
		num = (rand() % (upper - lower + 1)) + lower;
		for(j=0;j<attr;j++)
		{
			if(a[0][j]==b[m][k])
			{
				a_rand[i][k]=a[num][j];
				k++;	
			}
		}
	}
}

void testmodify(int **test,int **test_mod,int obj,int b[forest][26],int iii,int attr)
{
	int i,j,k;
	for(i=0;i<attr;i++)
	test_mod[0][i]=b[iii][i];

	k=0;
	for(j=0;j<25;j++)
	{
		if(test[0][j]==b[iii][k])
		{
			test_mod[1][k]=test[obj][j];
			k++;
		}
	}
}


int main()
{ 
    int i,it;         
    int obj,attr;//,class;//,x,*d,training, *test_value,class;

	int **a = (int **)malloc(1500 * sizeof(int *)); 
	for (it=0; it<1500; it++) 
	a[it] = (int *)malloc(26 * sizeof(int)); 			   

	int **a_rand = (int **)malloc(1500 * sizeof(int *)); 
	for (it=0; it<1500; it++) 
	a_rand[it] = (int *)malloc(26 * sizeof(int)); 	    

	read_input(a,"train_data.txt");	

	int b[forest][26];
	attr_perm(a,b,25);
	clock_t start, end;

    double cpu_time_used1,cpu_time_used2=0.0;
	struct deci_tree *parent[forest];

	for(i=0;i<forest;i++)
	{
		randomfile(a,a_rand,b,i,N1,25);
		parent[i] =(struct deci_tree *)malloc(sizeof(struct deci_tree));
		cpu_time_used1=0.0;

 		start = clock(); 	
		infoGainRecursive(a_rand,parent[i],N1,W);
		end = clock();
 		cpu_time_used1 = ((double) (end - start))/CLOCKS_PER_SEC;
		cpu_time_used2+=cpu_time_used1;	
	}
	free(a);
	free(a_rand);

	printf("\n time=  %f  milliseconds\n",cpu_time_used2*1000);
 	printf("hi %d\n",hi);
	int **test = (int **)malloc(1500 * sizeof(int *)); 
	for (it=0; it<1500; it++) 
	test[it] = (int *)malloc(26 * sizeof(int)); 
	read_input(test,"test_data.txt");
	cpu_time_used2=0.0;		
	attr=W;

	int classes,classes1,classes2;
	double counting=0;
	for(obj=1;obj<N2;obj++)
	{
		classes1=0;
		classes2=0;
		for(i=0;i<forest;i++)
		{
			int **test_mod = (int **)malloc(2 * sizeof(int *)); 
			for (it=0; it<2; it++) 
			test_mod[it] = (int *)malloc(26 * sizeof(int)); 
		
			testmodify(test,test_mod,obj,b,i,attr);

			cpu_time_used1=0.0;
 			start = clock();   
			classes=classification(test_mod,1,attr,parent[i]);
			end = clock();
 			cpu_time_used1 = ((double) (end - start))/CLOCKS_PER_SEC;
			cpu_time_used2+=cpu_time_used1;

			if(classes==0)
			classes1++;
			if(classes==1)
			classes2++;
		}
		if(classes1>classes2)
		classes=0;
		else
		classes=1;			
		if(test[obj][24]==classes)
		counting+=1;
	}
	printf("%lf\n",(counting/(N2-1)));
	printf("\n prediction time=  %f  milliseconds\n",cpu_time_used2*1000 );
	printf("findk_t %lf\n sum_t %lf\n entropy_t %lf \nmax_t %lf\n",findk_t*1000,sum_t*1000,entropy_t*1000,max_t*1000 );
    return 0;	
}