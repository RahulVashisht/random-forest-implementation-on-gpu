#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <string>
#include <fstream>
#include <sstream>
#include "gputimer.h"
#include <cuda_profiler_api.h>

#include "math.h"
#include "limits.h"

#define forest 1

#define N1 315
#define W 25

#define MINIMUM -99
#define trainingData "train_data.txt"
#define testingData "test_data.txt"
#define M 13
#define N 944
#define trainFileData(row,col) trainFileData[row*M+col]
#define testFileData(row,col) testFileData[row*M+col]

using namespace std;

vector <vector <int> > trainFile;
vector <vector <int> > testFile;

GpuTimer kernelTimer;
GpuTimer mallocTimer;
float kernelTime = 0;
float mallocTime = 0;

int *d_trainFileData, *d_cardinality;
float *infoGainsInitializer;
__device__ float d_infoGainOfData;

dim3 blocks(M);
dim3 threads(N);

struct Node{
	int numOfChildren;
	int val;
	int branchVal;
	int attribute;
	struct Node *child[50];
};

typedef struct Node node;

// initialising tree node
node* create(){
	node* n = new node;
	n->numOfChildren = 0;
	n->attribute = -1;
	n->val = -1;
	n->branchVal = -1;
	return n;
}

// function to read data and store in fileContent & testFileContent vectors(2d)
void readCSV(string str)
{
	if(str.compare("training")==0){
		ifstream ifs(trainingData);
		string line;

		// read from ifs into string 'line'
		while(getline(ifs,line)){
			stringstream lineStream(line);
			string cell;
			vector <int> values;
			// collecting row data from file delimited by ','
			while(getline(lineStream,cell,',')){
				const char *cstr = cell.c_str();
				values.push_back(atoi(cstr));
			}
			trainFile.push_back(values);
		}
		ifs.close();
	}
	else if(str.compare("testing")==0){
		ifstream ifs(testingData);
		string line;
		
		// read from ifs into string 'line'
		while(getline(ifs,line)){
			stringstream lineStream(line);
			string cell;
			vector <int> values;
			// collecting row data from file delimited by ','
			while(getline(lineStream,cell,',')){
				const char *cstr = cell.c_str();
				values.push_back(atoi(cstr));
			}
			testFile.push_back(values);
		}
		ifs.close();
	}
}

__global__ void getInfoGains(int *attr,int *data,int dataSize,float *infoGains,int *trainFileData,int *cardinality)
{
	if(attr[blockIdx.x]==0 &&  blockIdx.x!=M-1){
		int tid,bid,j;
		tid=threadIdx.x;
		bid=blockIdx.x;
		__shared__ int attrValCount[30];
		__shared__ int classAttrValCount[100][100];
		if(tid<100){
			attrValCount[tid]=0;
			for(j=0;j<100;j++){
				classAttrValCount[tid][j]=0;
			}
		}
		__syncthreads();
		int classVal = trainFileData(data[tid],M-1);
		int attrVal = trainFileData(data[tid],bid);
		atomicAdd(&attrValCount[attrVal],1);
		atomicAdd(&classAttrValCount[attrVal][classVal],1);
		__syncthreads();

		if(tid==0){
			int i,j;
			float infoGain,intermediateGain;
			infoGain=0;
			for(i=1;i<=cardinality[bid];i++){
				intermediateGain=0;
				if(attrValCount[i]==0){
					continue;
				}
				for(j=1;j<=cardinality[M-1];j++){
					if(classAttrValCount[i][j]==0){
						continue;
					}
					intermediateGain+=(float(classAttrValCount[i][j])/(float)attrValCount[i])*(log((float)classAttrValCount[i][j]/(float)attrValCount[i])/log((float)2));
				}
				intermediateGain*=(float(attrValCount[i])/(float)dataSize);
				infoGain-=intermediateGain;
			}
			infoGains[bid]=infoGain;
		}
	}
}

__global__ void getInfoGainOfData(int *data,int dataSize,int *trainFileData,int *cardinality)
{
	__shared__ int classValCount[10];
	int classVal,i,tid;
	float infoGain;
	tid=threadIdx.x;
	if(tid<10){
		classValCount[tid]=0;
	}
	__syncthreads();
	classVal=trainFileData(data[threadIdx.x],M-1);
	atomicAdd(&classValCount[classVal],1);
	__syncthreads();
	if(tid==0){
		infoGain=0;
		for(i=0;i<=cardinality[M-1];i++){
			if(classValCount[i]==0){
				continue;
			}
			infoGain+=((float)classValCount[i]/(float)dataSize)*(log((float)classValCount[i]/(float)dataSize)/log((float)2));
		}
		d_infoGainOfData=-1*infoGain;
	}
}

int popularVote(int *data,int dataSize)
{
	int i,outputClass,ans,maxVal;
	map <int, int> dataCount;
	map <int, int>::iterator it;
	for(i=0;i<dataSize;i++){
		outputClass = trainFile[data[i]][M-1];
		if(dataCount.find(outputClass)==dataCount.end()){
			dataCount.insert(make_pair(outputClass,1));
		}
		else{
			dataCount[outputClass]++;
		}
	}
	maxVal = MINIMUM;
	for(it=dataCount.begin();it!=dataCount.end();it++){
		if(it->second > maxVal){
			ans = it->first;
		}
	}
	return ans;
}

void decision(int *h_attr,int *h_data, node *root,int h_dataSize)
{
	int flag,h_selectedAttribute,i;
	float maxGain;
	if(h_dataSize==0){
		return;
	}
	flag=1;
	for(i=1;i<h_dataSize;i++){
		if(trainFile[h_data[i]][M-1]!=trainFile[h_data[i-1]][M-1]){
			flag=0;
			break;
		}
	}
	if(flag==1){
		root->val=trainFile[h_data[0]][M-1];
		return;
	}

	int *d_attr, *d_data;
	float *d_infoGains;
	float h_infoGains[M];
	float h_infoGainOfData;

	mallocTimer.Start();
	cudaMalloc((void**)&d_attr,M*sizeof(int));
	cudaMalloc((void**)&d_data,h_dataSize*sizeof(int));
	cudaMalloc(&d_infoGains,M*sizeof(float));
	cudaMemcpy((void*)d_attr,(void*)h_attr,M*sizeof(int),cudaMemcpyHostToDevice);
	cudaMemcpy((void*)d_data,(void*)h_data,h_dataSize*sizeof(int),cudaMemcpyHostToDevice);
	cudaMemcpy(d_infoGains, infoGainsInitializer, M*sizeof(float),cudaMemcpyHostToDevice);
	mallocTimer.Stop();
	mallocTime+=mallocTimer.Elapsed();

	kernelTimer.Start();
	getInfoGains<<<blocks,h_dataSize>>>(d_attr,d_data,h_dataSize,d_infoGains,d_trainFileData,d_cardinality);
	kernelTimer.Stop();
	kernelTime+=kernelTimer.Elapsed();

	mallocTimer.Start();
	cudaMemcpy((void*)h_infoGains,(void*)d_infoGains,M*sizeof(float),cudaMemcpyDeviceToHost);

	cudaFree(d_attr);
	cudaFree(d_infoGains);
	mallocTimer.Stop();
	mallocTime+=mallocTimer.Elapsed();	

	kernelTimer.Start();
	getInfoGainOfData<<<1,h_dataSize>>>(d_data,h_dataSize,d_trainFileData,d_cardinality);
	kernelTimer.Stop();
	kernelTime+=kernelTimer.Elapsed();
	
	mallocTimer.Start();
	cudaMemcpyFromSymbol(&h_infoGainOfData,d_infoGainOfData,sizeof(float),0,cudaMemcpyDeviceToHost);

	cudaFree(d_data);
	mallocTimer.Stop();
	mallocTime+=mallocTimer.Elapsed();

	maxGain=MINIMUM;
	h_selectedAttribute=-1;
	for(i=0;i<M-1;i++){
		if(h_attr[i]==0){
			h_infoGains[i]=h_infoGainOfData-h_infoGains[i];
			if(h_infoGains[i]>maxGain){
				maxGain=h_infoGains[i];
				h_selectedAttribute=i;
			}
		}
	}
	
	root->attribute = h_selectedAttribute;
	h_attr[h_selectedAttribute]=1;

	if(h_selectedAttribute==-1){
		root->val = popularVote(h_data, h_dataSize);
		return;
	}

	map<int, vector <int> > dividedData;
	map<int, vector <int> >::iterator it;
	int attrVal;

	for(i=0;i<h_dataSize;i++){
		attrVal = trainFile[h_data[i]][h_selectedAttribute];
		if(dividedData.find(attrVal) == dividedData.end()){
			vector <int> x;
			x.push_back(h_data[i]);
			dividedData.insert(make_pair(attrVal,x));
		}
		else{
			dividedData[attrVal].push_back(h_data[i]);
		}
	}
	for(i=0,it=dividedData.begin();it!=dividedData.end();it++,i++){
		root->numOfChildren++;
		node* childNode;
		childNode = create();
		childNode->branchVal = it->first;
		root->child[i] = childNode;
		int* h_childData = &(it->second[0]);

		int new_attr[M];
		for(int z=0;z<M;z++){
			new_attr[z]=h_attr[z];
		}

		decision(new_attr, h_childData, childNode, it->second.size());
	}
}

__global__ void getCardinality(int *trainFileData,int *cardinality)
{
	__shared__ int x[100];
	int bid,tid,i;
	bid=blockIdx.x;
	tid=threadIdx.x;
	if(tid<100){
		x[tid]=0;
	}
	__syncthreads();
		x[trainFileData(tid,bid)]=1;
		__syncthreads();
		for(i=1;i<100;i*=2){
			int index = 2*i*tid;
			if(index+i<100){
				x[index]+=x[index+i];
			}
			__syncthreads();
		}
		if(tid==0){
			cardinality[bid]=x[0];
		}
	
	__syncthreads();

}

// function for testing decision tree
//test(test_mod,root[i]);
int testt(int *test,node* root)
{
	int attr,attrVal,j,flag;
	node* temp;
	temp=root;
	flag=0;
		//traverse decision tree
	while(temp->val==-1 && temp->attribute!=-1)
	{
		attr = temp->attribute;
		attrVal=test[attr];
		for(j=0;j<temp->numOfChildren;j++)
		{
				if(temp->child[j]->branchVal == attrVal)
				{
					break;
				}
		}
		if(j==temp->numOfChildren)
		{
				flag=1;
				break;
		}
		else
		{
				temp=temp->child[j];
		}
	}
	//if(temp->val>=0)
	return(temp->val);
	//return(0);
}

void attr_perm(int b[forest][M])
{
	int i,j,temp;

	for(i=0;i<forest;i++)
	{
		b[i][M-1]=(W-1);
	}

	for(i=0;i<forest;i++)
	{
		for(j=0;j<(M-1);j++)
		{
			temp=rand();
			temp=temp%2;
			temp+=(2*j);
			b[i][j]=temp;
		}
	}
}

void randomfile(int h_trainFileData[N*M], int b[forest][M], int obj)
{
	int i,j,num, upper=N-1,lower=0;
	//srand(time(0)); 
	for(i=0;i<N;i++)
	{	
		num = (rand() % (upper - lower + 1)) + lower; 
		for(j=0;j<M;j++)
		{
			h_trainFileData[(i*M)+j] = trainFile[num][b[obj][j]];

		}
	}
}
	


void testmodify(int test[N1][W],int *test_mod,int obj,int b[forest][M],int iii)
{
	int i;
	for(i=0;i<M;i++)
	{
	test_mod[i]=test[obj][b[iii][i]];
	}	
}

int main()
{
    cudaEvent_t start;
    cudaEvent_t stop;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);
 
	float timing=0;
	GpuTimer timer;

	int i,loop,j;
	node* root[forest];

	readCSV("training");

	int b[forest][M];
	attr_perm(b);

	int h_trainFileData[N*M];


	int h_data[N],h_attr[M];
	cudaMalloc((void**)&d_trainFileData,N*M*sizeof(int));
	cudaMalloc((void**)&d_cardinality,M*sizeof(int));
	infoGainsInitializer = (float*)malloc(M*sizeof(float));

	for(loop =0;loop<forest;loop++)
	{
		for(i=0;i<N;i++){
			h_data[i]=i;
		}

		for(i=0;i<M;i++){
			h_attr[i]=0;
		}
		
		randomfile(h_trainFileData,b,loop);
		mallocTimer.Start();
		
		cudaMemcpy((void*)d_trainFileData,(void*)h_trainFileData,M*N*sizeof(int),cudaMemcpyHostToDevice);

		cudaMemset(d_cardinality,0,M*sizeof(int));
		mallocTimer.Stop();
		mallocTime+=mallocTimer.Elapsed();

		kernelTimer.Start();
		getCardinality<<<blocks,threads>>>(d_trainFileData,d_cardinality);
		kernelTimer.Stop();
		kernelTime+=kernelTimer.Elapsed();

		root[loop] = create();

		for(i=0;i<M;i++){
			infoGainsInitializer[i]=MINIMUM;
		}

		float elapsed=0;
        cudaEventRecord(start, 0);
		decision(h_attr,h_data,root[loop],N);
	 	cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
        cudaEventElapsedTime(&elapsed, start, stop);
 
		timing+=elapsed;
	}

		cudaProfilerStop();

	cudaFree(d_trainFileData);
	cudaFree(d_cardinality);

	float  predict=0,predic_time=0;

	int test[N1][W],obj;
	readCSV("testing");

	for(i=0;i<N1;i++)
	for(j=0;j<W;j++)
	test[i][j]=testFile[i][j];


	int classes,classes1,classes2;
	double counting=0;
	for(obj=0;obj<N1;obj++)
	{
		classes1=0;
		classes2=0;

		for(i=0;i<forest;i++)
		{
			int *test_mod = (int*)malloc(M * sizeof(int)); 		
			testmodify(test,test_mod,obj,b,i);

			predict=0;
			cudaEventRecord(start,0);		
			classes=testt(test_mod,root[i]);

			cudaEventRecord(stop);
    		cudaEventSynchronize(stop);
    		cudaEventElapsedTime(&predict, start, stop);
			predic_time+=predict;

			if(classes==0)
			classes1++;
			if(classes==1)
			classes2++;
		}
		if(classes1>classes2)
		classes=0;
		else
		classes=1;		
		
		if(test[obj][24]==classes)
		counting+=1;
	}

	printf("%lf\n",(counting/N1));
	printf("\n prediction time=  %f  milliseconds\n",predic_time);



	printf("Time taken: %gms\n",timing);
	printf("Kernel Time: %gms\n",kernelTime);
	printf("Malloc Time: %gms\n",mallocTime);

	return 0;
}