#include<stdio.h>
#include<string.h>
#include<math.h>
#include<time.h>
#include<stdlib.h>
#define forest 50
#define N1 945
#define N2 316
#define W 13
float findk_t=0.0,sum_t=0.0,max_t=0.0,entropy_t=0.0,temp_t=0.0;
cudaEvent_t start1, stop1;
int hi=0;
 
struct deci_tree
{
	int  colname; //attrbute name
	int result;  // class
	int childno;  // no of child
	int value;   // for classification value
	struct deci_tree *c[100]; // all child
	int ischild;  //  for child
}root;


__global__ void findk(int *a, int *b,int obj,int attr)//int b[50][100]
{       
    int Id = threadIdx.x + blockDim.x * blockIdx.x;
    int threadId=threadIdx.x;
	int r,c;
	int i,j,count;
	int flag;
	if(Id==0)
	{
	 r=50;
	 c=100;
	 count = 1;
	 flag  =0 ;
	}
	__syncthreads();

	if(Id<attr)
	{
	b[Id*c+1] = a[1*attr+Id];
	i = 1; 
	while(i < obj){
		for(j = 1 ; j < count; j++)
			{
		if(a[i*attr+Id] == b[Id*c+j]) 
		{
			flag = 1;     
		}
		}
		if(flag  == 0 ){
		b[Id*c+count] = a[i*attr+Id];
		count++;
		}
		flag =0;
		i++;

	}
		b[Id*c+0] = count-1;
		count = 1;  
	} 
}
void findk(int **a, int **b,int obj,int attr)
{
	int *h_a;
    cudaMallocHost((void **) &h_a, sizeof(int)*1500*26);
		int i=0,k=0;
			for(i=0;i<obj;i++)
			{
			    for(k=0;k<attr;k++)
			    {
			        h_a[i*attr+k]=a[i][k];
			    }
			}
	int *h_b;
    cudaMallocHost((void **) &h_b, sizeof(int)*100*100);
		
			for(i=0;i<100;i++)
			{
			    for(k=0;k<100;k++)
			    {
			        h_b[i*100+k]=b[i][k];
			    }
			}
    int *d_a, *d_b;
    cudaMalloc((void **) &d_a, sizeof(int)*1500*26);
    cudaMalloc((void **) &d_b, sizeof(int)*100*100);
    cudaMemcpy(d_a,h_a, sizeof(int)*26*1500, cudaMemcpyHostToDevice);
    memset(d_b,100*100*sizeof(int), 0);
	temp_t=0.0;
	cudaEventRecord(start1,0);
	findk<<<1,attr>>>(d_a, d_b, obj, attr);
	cudaEventRecord(stop1);
	cudaEventSynchronize(stop1);
	cudaEventElapsedTime(&temp_t, start1, stop1);
	findk_t+=temp_t;
	cudaMemcpy(h_b, d_b, sizeof(int)*100*100, cudaMemcpyDeviceToHost);
	for(i=0;i<100;i++)
		{
		    for(k=0;k<100;k++)
		    {
		       b[i][k]= h_b[i*100+k];
		    }
		}
}
__global__ void max1(double* input, int* C, int *I,int BLOCK_SIZE)
{
	extern __shared__ double  partialSum[];
	int t = threadIdx.x + blockDim.x * blockIdx.x;
	I[t]=t;
	I[t*2]=t*2; 
	partialSum[t] = input[t];
	partialSum[BLOCK_SIZE+t] = input[BLOCK_SIZE+t];
    for (int stride =1; stride <= BLOCK_SIZE; stride *=2) 
	{  
		 __syncthreads();
		if (t % stride ==0)
	         {
		    if(partialSum[2*t]< partialSum[2*t+stride])
		    {
		    partialSum[2*t]=partialSum[2*t+stride]; 
		    I[2*t]=I[2*t+stride];
		    }
		 } 
	}
    C[0]=I[0];	
}


int findmax(double *h_A,int N)
{
	for(int i=N;i<100;i++)
	h_A[i]=0.0;
	int BLOCK_SIZE=32;
	size_t size = 100 * sizeof(double);
	int * h_C = (int*)malloc(1*sizeof(int));
	double* d_A; cudaMalloc(&d_A, size);
	int * d_I; cudaMalloc(&d_I, 100 * sizeof(int));
	int * d_C; cudaMalloc(&d_C, 1*sizeof(int));
	cudaMemcpy(d_A, h_A, size,cudaMemcpyHostToDevice);

	temp_t=0.0;
	cudaEventRecord(start1,0);	
	max1<<<1,BLOCK_SIZE,100*sizeof(double)>>>(d_A, d_C,d_I,BLOCK_SIZE);
	cudaEventRecord(stop1);
	cudaEventSynchronize(stop1);
	cudaEventElapsedTime(&temp_t, start1, stop1);
	max_t+=temp_t;
	cudaMemcpy(h_C, d_C, sizeof(int),cudaMemcpyDeviceToHost);
	cudaFree(d_A);
	cudaFree(d_C);  
	return h_C[0];
}
__global__ void findsum1(double* input, double* C, int BLOCK_SIZE)
{  
	extern __shared__ double  partialSum[];
	int t = threadIdx.x + blockDim.x * blockIdx.x;
	partialSum[t] = input[t];
	partialSum[BLOCK_SIZE+t] = input[BLOCK_SIZE+t];
    for (int stride =1; stride <= BLOCK_SIZE; stride *=2) 
	{  
		 __syncthreads();
		if (t % stride ==0) 
		partialSum[2*t]+= partialSum[2*t+stride]; 
	}
    C[0]=partialSum[0];	
}

double findsum(double *h_A,int N)
{
	for(int i=N;i<100;i++)
	h_A[i]=0.0;
	int BLOCK_SIZE=32;
	size_t size = 100 * sizeof(double);
	double* h_C = (double*)malloc(1*sizeof(double));
	double* d_A; cudaMalloc(&d_A, size);
	double* d_C; cudaMalloc(&d_C, sizeof(double));
	cudaMemcpy(d_A, h_A, size,cudaMemcpyHostToDevice);

	temp_t=0.0;
	cudaEventRecord(start1,0);
	findsum1<<<1, BLOCK_SIZE,100*sizeof(double)>>>(d_A, d_C, BLOCK_SIZE);
	cudaEventRecord(stop1);
	cudaEventSynchronize(stop1);
	cudaEventElapsedTime(&temp_t, start1, stop1);
	sum_t+=temp_t;

	cudaMemcpy(h_C, d_C, sizeof(double),cudaMemcpyDeviceToHost);
	cudaFree(d_A);
	cudaFree(d_C);  
    return h_C[0];
}
__global__ void entropy(int *a,int obj,int attr,int attrpos,int val,double *temp,int *d_a)// i added d instead of return
{
	int Id=threadIdx.x + blockDim.x * blockIdx.x;
	__shared__  int x,k;           
	__shared__   double b,c,total;
	__shared__   double count1,count2 ;
	__shared__  int count11,count22;
	if(Id==0)
	{
		x = d_a[1];
		total= 1.0;
		count11=0;
		count22=0;
		count1 = 0.0;
		count2 = 0.0 ;
	}
	__syncthreads();
	if(Id!=0 && Id<obj)
	{
		if(attrpos == attr-1)
		{
			if(a[Id]==x)
			{
			atomicAdd(&count11,1);
			}   else
			atomicAdd(&count22,1);
		}
		if( attrpos != attr-1)
		{
			if(a[Id] == val)
			{
				if(d_a[Id] == x)
								    
				atomicAdd(&count11,1);
				else
				atomicAdd(&count22,1);
			}
		}
	}
	 __syncthreads();
	if(Id==0)
	{
		count1=count11*1.0;
		count2=count22*1.0;
		b = count1/(count1+count2);
		c = count2/(count1+count2);
		temp[0]=count1+count2;

		if(count1 == 0.000000 || count2 == 0.000000)
		temp[1] = 0 ;
		else
		temp[1] =  ((count1+count2)/(obj-1)) *(-(b*(logf(b)/logf(2)) + c*(logf(c)/logf(2))));
	}
}

double entropy(int **a,int obj,int attr,int attrpos,int val,double *temp)
{
	int *h_a;
    cudaMallocHost(&h_a, obj*sizeof(int));
	int *h_a1;
    cudaMallocHost(&h_a1, obj*sizeof(int));
	int i=0,k=0;
    for(i=0;i<obj;i++)
    {
		h_a[i]=a[i][attrpos];
		h_a1[i]=a[i][attr-1];
    }

    int *d_a;
    cudaMalloc(&d_a, sizeof(int)*obj);
    int *d_a1;
    cudaMalloc(&d_a1, sizeof(int)*obj);
    cudaMemcpy(d_a,h_a, sizeof(int)*obj, cudaMemcpyHostToDevice);
    cudaMemcpy(d_a1,h_a1, sizeof(int)*obj, cudaMemcpyHostToDevice);
	double *dev_temp;
    cudaMalloc((void **) &dev_temp, 2*sizeof(double));
	double *hos_temp;
    cudaMallocHost((void **) &hos_temp, sizeof(double)*2);
	temp_t=0.0;
	cudaEventRecord(start1,0);
	entropy<<<1,obj>>>(d_a,obj,attr,attrpos,val,dev_temp,d_a1);
	cudaEventRecord(stop1);
	cudaEventSynchronize(stop1);
	cudaEventElapsedTime(&temp_t, start1, stop1);
	entropy_t+=temp_t;
	cudaMemcpy(hos_temp,dev_temp, sizeof(double)*2, cudaMemcpyDeviceToHost);
	*temp=hos_temp[0];
	return hos_temp[1];
}

void infoGainRecursive(int **a,struct deci_tree *parent,int obj,int attr)
{
	int i,j,k,l,m,it;
	int var1;
	double temp;
	
	int **modifiedA = (int **)malloc(100 * sizeof(int *));
	for (it=0; it<100; it++)
	modifiedA[it] = (int *)malloc(100 * sizeof(int));

	int **modifiedB = (int **)malloc(100 * sizeof(int *)); 
	for (it=0; it<100; it++) 
	modifiedB[it] = (int *)malloc(100 * sizeof(int)); 		

    double *col = (double *)malloc(100 * sizeof(double )); 
    double *entr = (double *)malloc(100 * sizeof(double )); 
    double *gain ;//cudaHostAlloc( &gain, 100 * sizeof(double ),0);//
	gain= (double *)malloc(100 * sizeof(double )); 

	findk(a,modifiedB,obj,attr);
	entr[attr-1] = entropy(a,obj,attr,attr-1,0,&temp);
	if(entr[attr-1]!=0 && attr>1)
	{	
			parent->result=3;
    		parent->childno=0;
    		parent->ischild=0;;
		hi++;
	   	for(i= 0; i < attr-1 ; i++)
		{
			for(j= 1 ; j <= modifiedB[i][0]; j++)
			{   	
				col[j-1] = entropy(a,obj,attr,i,modifiedB[i][j],&temp);
				col[j-1]*=(temp/(obj-1));
				modifiedA[i][j]=temp+10;    
			}
		        entr[i] =  findsum(col,modifiedB[i][0]);   
			    gain[i] =  entr[attr-1]  - entr[i];   
		}	
		var1= findmax(gain,attr-1);
		for(j= 1 ; j <=modifiedB[var1][0] ; j++)
		{   
			int **arr = (int **)malloc(modifiedA[var1][j] * sizeof(int *)); 
			for (it=0; it<modifiedA[var1][j]; it++) 
			arr[it] = (int *)malloc(attr * sizeof(int)); 
			for(m=0,k=0;m<attr;m++)
			{
				if(m!=var1)
				{
					arr[0][k]=a[0][m];
					k++;
				}
			}
				k=1;
				l=0;
			for(i=1;i<obj;i++)
			{
				if(a[i][var1]==modifiedB[var1][j])
				{
					for( m=0;m<attr;m++)
					{
						if(m!=var1)
						{
							arr[k][l]=a[i][m];
							l++;
						}
					}	
				k++;
				l=0;
				}
			}
			struct deci_tree *temp;
			temp =(struct deci_tree *)malloc(sizeof(struct deci_tree));
			parent->colname=a[0][var1];
			parent->c[parent->childno]=temp;
			parent->c[parent->childno]->value=modifiedB[var1][j];	
			parent->childno++;    

			infoGainRecursive(arr,temp,modifiedA[var1][j]-9,attr-1);
    	}
	free(modifiedB);
	free(modifiedA);
	free(col);
	free(entr);
	free(gain);
    }
	else if(attr>1)
	{
		parent->result=a[1][attr-1];
    	parent->ischild=1;
		free(modifiedB);
		free(modifiedA);
		free(col);
		free(entr);
		free(gain);
	}
	else
	{
		free(modifiedB);
		free(modifiedA);
		free(col);
		free(entr);
		free(gain);
	}
}

void read_input(int  **M,  char *filename)
{
    FILE * ipf = fopen(filename,  "r" );
	char * line = NULL;
    size_t len = 0;
    ssize_t read;
	int k1=0,k2=0,k3=0,k4=24,j;
	while ((read = getline(&line, &len, ipf)) != -1)
	{
		k2=0;
		int sum=0,mul=1;	
		for( j=read-2;j>=-1 ; j--)
		{
			if(line[j]==',' || j<0)
			{
				M[k3][k4]=sum;
				k4--;
				k2++;
				sum=0;
				mul=1;	
			}
			else
			{
				int temp=((int)(line[j]-'0'));
				sum+=(temp*mul);
				mul*=10;
			}
		}
		k1++;
		k3++;
		k4=24;
	}
}

int classification(int **test,int obj, int attr,struct deci_tree *parent)
{
	if(parent->ischild==1)
	{
		return(parent->result);
	}
	else
	{
		int index=0;
		for(;index<attr;index++)
		{
			if(parent->colname==test[0][index])
			{
				break;
			}
		}
		int i;
		for( i=0; i<parent->childno; i++)		
		{
			if(parent->c[i]->value==test[obj][index])
			{
				classification(test,obj,attr,parent->c[i]);

				break;
			}
		}
	}
}

void attr_perm(int **a, int b[forest][26], int attr)
{
	int i,j,temp;//,arr[24];

	for(i=0;i<forest;i++)
	{
		b[i][W-1]=a[0][24];
	}
	for(i=0;i<forest;i++)
	{
		for(j=0;j<(W-1);j++)
		{
			temp=rand();
			temp=temp%2;
			temp+=(2*j);
			b[i][j]=temp+100;
		}
	}
}

void randomfile(int **a,int **a_rand,int b[forest][26],int m,int obj,int attr)
{
	int i,j,k,num, upper=obj-1,lower=1;
	for(i=0;i<=(W-1);i++)
	a_rand[0][i]=b[m][i];
	
	for(i=1;i<obj;i++)
	{
		k=0;
		num = (rand() % (upper - lower + 1)) + lower;
		for(j=0;j<attr;j++)
		{
			if(a[0][j]==b[m][k])
			{
				a_rand[i][k]=a[num][j];
				k++;	
			}
		}
	}
}

void testmodify(int **test,int **test_mod,int obj,int b[forest][26],int iii,int attr)
{
	int i,j,k;
	for(i=0;i<attr;i++)
	test_mod[0][i]=b[iii][i];

	k=0;
	for(j=0;j<25;j++)
	{
		if(test[0][j]==b[iii][k])
		{
			test_mod[1][k]=test[obj][j];
			k++;
		}
	}
}

int main()
{ 
	cudaEventCreate(&start1);
	cudaEventCreate(&stop1);
    int i,it;         
    int obj,attr;//,class;//,x,*d,training, *test_value,class;

	int **a = (int **)malloc(1500 * sizeof(int *)); 
	for (it=0; it<1500; it++) 
	a[it] = (int *)malloc(26 * sizeof(int)); 			   

	int **a_rand = (int **)malloc(1500 * sizeof(int *)); 
	for (it=0; it<1500; it++) 
	a_rand[it] = (int *)malloc(26 * sizeof(int)); 	    

	read_input(a,"train_data.txt");	

	int b[forest][26];
	attr_perm(a,b,25);
	clock_t start, end;

    double cpu_time_used1,cpu_time_used2=0.0;
	struct deci_tree *parent[forest];

	for(i=0;i<forest;i++)
	{
		randomfile(a,a_rand,b,i,N1,25);
		parent[i] =(struct deci_tree *)malloc(sizeof(struct deci_tree));
		cpu_time_used1=0.0;

 		start = clock(); 	
		infoGainRecursive(a_rand,parent[i],N1,W);
		end = clock();
 		cpu_time_used1 = ((double) (end - start))/CLOCKS_PER_SEC;
		cpu_time_used2+=cpu_time_used1;	
	}
	free(a);
	free(a_rand);

	printf("\n time=  %f  milliseconds\n",cpu_time_used2*1000);
	printf("hi %d\n",hi);

	int **test = (int **)malloc(1500 * sizeof(int *)); 
	for (it=0; it<1500; it++) 
	test[it] = (int *)malloc(26 * sizeof(int)); 
	read_input(test,"test_data.txt");
	cpu_time_used2=0.0;		
	attr=W;

	int classes,classes1,classes2;
	double counting=0;
	for(obj=1;obj<N2;obj++)
	{
		classes1=0;
		classes2=0;
		for(i=0;i<forest;i++)
		{
			int **test_mod = (int **)malloc(2 * sizeof(int *)); 
			for (it=0; it<2; it++) 
			test_mod[it] = (int *)malloc(26 * sizeof(int)); 
		
			testmodify(test,test_mod,obj,b,i,attr);

			cpu_time_used1=0.0;
 			start = clock();   
			classes=classification(test_mod,1,attr,parent[i]);
			end = clock();
 			cpu_time_used1 = ((double) (end - start))/CLOCKS_PER_SEC;
			cpu_time_used2+=cpu_time_used1;

			if(classes==0)
			classes1++;
			if(classes==1)
			classes2++;
		}
		if(classes1>classes2)
		classes=0;
		else
		classes=1;			
		if(test[obj][24]==classes)
		counting+=1;
	}
	printf("%lf\n",(counting/(N2-1)));
	printf("\n prediction time=  %f  milliseconds\n",cpu_time_used2*1000 );
	printf("findk_t %lf\n sum_t %lf\n entropy_t %lf \nmax_t %lf\n",findk_t,sum_t,entropy_t,max_t );
    return 0;	
}